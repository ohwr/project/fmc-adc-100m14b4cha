# SPDX-FileCopyrightText: 2023 CERN (home.cern)
#
# SPDX-License-Identifier: LGPL-2.1-or-later

files = [
    "sourceid_{}_pkg.vhd".format(syn_top),
]

try:
    exec(open(fetchto + "/general-cores/tools/gen_sourceid.py").read(),
         None, {'project': syn_top})
except Exception as e:
    print("Error: cannot generate source id file")
    raise
